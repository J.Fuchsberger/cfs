<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://www.cfs++.org/simulation"
  xmlns="http://www.cfs++.org/simulation"
  xmlns:cfs="http://www.cfs++.org/simulation"
  xmlns:mat="http://www.cfs++.org/material"
  elementFormDefault="qualified">

  <xsd:import namespace="http://www.cfs++.org/material"
              schemaLocation="../CFS-Material/CFS_Material.xsd"/>

  <xsd:annotation>
  	<xsd:documentation xml:lang="en">
      Parameter file schema for the Coupled Field Solver project CFS++
    </xsd:documentation>
  </xsd:annotation>

  <xsd:include schemaLocation="Schemas/CFS_Misc.xsd" />
  <!-- ******************************************************************* -->
  <!--   Include definition of PDE types and elements -->
  <!-- ******************************************************************* -->
  <xsd:include schemaLocation="Schemas/CFS_PDEbasic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEelectrostatic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEmechanic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEacoustic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEsplit.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEacousticMixed.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEmagnetic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEmagedge.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEsmooth.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEheatConduction.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEfluidMech.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEfluidMechLin.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEelectricConduction.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEelecQuasistatic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEwaterWave.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDEtest.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PDELatticeBoltzmann.xsd"/>

	<!-- ******************************************************************* -->
	<!--   Include definition of coupling types and elements -->
	<!-- ******************************************************************* -->
	<xsd:include schemaLocation="Schemas/CFS_Coupling.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_DirectCoupling.xsd" />


	<!-- ******************************************************************* -->
	<!--   Include further definitions -->
	<!-- ******************************************************************* -->
	<xsd:include schemaLocation="Schemas/CFS_PostProc.xsd" />
  <xsd:include schemaLocation="Schemas/CFS_HystereticSystem.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_LinearSystem.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_Solvers.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_Preconds.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_Analysis.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_Domain.xsd" />
  <xsd:include schemaLocation="Schemas/CFS_Documentation.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_PythonKernel.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_FileFormats.xsd" />
	<xsd:include schemaLocation="Schemas/CFS_FePolynomial.xsd"/>
	<xsd:include schemaLocation="Schemas/CFS_IntegrationScheme.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_ErsatzMaterial.xsd"/>
  <xsd:include schemaLocation="Schemas/CFS_Optimization.xsd"/>
  
	<!-- ******************************************************************* -->
	<!--   Definition of enclosing main element -->
	<!-- ******************************************************************* -->
	<xsd:element name="cfsSimulation">
		<xsd:complexType>
			<xsd:sequence>
        <!-- Allows to provide arbitrary info which is copied to .info.xml - similar to "id" attribute  -->     
        <xsd:element name="info" minOccurs="0">
          <xsd:complexType>
            <xsd:sequence>
              <xsd:any minOccurs="0" namespace="##any" processContents="skip"/>
            </xsd:sequence>
          </xsd:complexType>
        </xsd:element>

        <!-- Tags for documenting simulations (required just for test suite)-->
        <xsd:element name="documentation" type="DT_Documentation" minOccurs="0"/>

        <!-- Optional python kernel -->
        <xsd:element name="python" type="DT_PythonKernel" minOccurs="0"/>

        <!-- File format list for io-specifications (required)-->
        <xsd:element name="fileFormats" type="DT_FileFormats" minOccurs="1"/>

				<!-- Description of problem domain (required) -->
				<xsd:element name="domain" type="DT_Domain">

					<!-- Ensure that names are unique over all sub-groups -->
					<xsd:key name="CS_DomainNamesTotal">
						<xsd:selector
							xpath="cfs:region|cfs:nodes|cfs:elements" />
						<xsd:field xpath="@name" />
					</xsd:key>

					<!-- Generate key for check that regions referenced by PDEs
						do actually exist. See 'CS_RegionExists' below -->
					<xsd:key name="CS_DomainNamesRegion">
						<xsd:selector xpath="cfs:region" />
						<xsd:field xpath="@name" />
					</xsd:key>

					<!-- Generate key for check that nodes referenced by PDEs
						do actually exist. See 'CS_NodesExist' below -->
					<xsd:key name="CS_DomainNamesNodes">
						<xsd:selector xpath="cfs:nodes" />
						<xsd:field xpath="@name" />
					</xsd:key>

					<!-- Generate key for check that elements referenced by PDEs
						do actually exist. See 'CS_ElemsExist' below -->
					<xsd:key name="CS_DomainNamesElems">
						<xsd:selector xpath="cfs:elements" />
						<xsd:field xpath="@name" />
					</xsd:key>
				</xsd:element>
				
				<!-- Definition of fe polynomials -->
				<xsd:element name="fePolynomialList" maxOccurs="1" minOccurs="0" type="DT_PolyList">
				</xsd:element>
				
				<xsd:element name="integrationSchemeList" maxOccurs="1" minOccurs="0">
          <xsd:complexType>
            <xsd:sequence>
              <xsd:element name="scheme" type="DT_IntegrationScheme"
                minOccurs="0" maxOccurs="unbounded" />
            </xsd:sequence>
          </xsd:complexType>         
        </xsd:element>
				<xsd:element name="sequenceStep" minOccurs="0" maxOccurs="unbounded">
					<xsd:complexType>
						<xsd:sequence>

							<!-- Type of analysis (required) -->
							<xsd:element name="analysis">
								<xsd:complexType>
									<xsd:sequence>
										<!-- Parameters for transient/harmonic analysis (semi-optional) -->
										<xsd:element ref="AnalysisBasic" minOccurs="1" maxOccurs="1" />
									</xsd:sequence>
								</xsd:complexType>
							</xsd:element>

							<!-- List of PDEs (required) -->
							<xsd:element name="pdeList">
								<xsd:complexType>
									<xsd:sequence>
										<xsd:element ref="PDEBasic" minOccurs="1" maxOccurs="unbounded" />
									</xsd:sequence>
								</xsd:complexType>
							</xsd:element>

							<!-- List of PDE Couplings (optional) -->
							<xsd:element name="couplingList" minOccurs="0" maxOccurs="unbounded">
								<xsd:complexType>
									<xsd:sequence>
										<xsd:element ref="PDEBasicCoupling" minOccurs="0" maxOccurs="unbounded" />
									</xsd:sequence>
									<xsd:attribute name="tag" type="xsd:token" use="optional" default="anyTag" />
								</xsd:complexType>
							</xsd:element>

							<!-- Data postprocessing section (optional) -->
							<xsd:element name="postProcList" type="DT_PostProcList" minOccurs="0" maxOccurs="1" />

							<!-- Paremeters for setting up and solving linear systems -->
							<xsd:element name="linearSystems" minOccurs="0">
								<xsd:complexType>
									<xsd:sequence>
										<xsd:element name="system" type="DT_SystemSpec" minOccurs="1" maxOccurs="unbounded" />
									</xsd:sequence>
								</xsd:complexType>
							</xsd:element>
						</xsd:sequence>
						<xsd:attribute name="index" type="xsd:positiveInteger" default="1" />
						<xsd:attribute name="usage" use="optional" default="startValue">
							<xsd:simpleType>
								<xsd:restriction base="xsd:token">
									<xsd:enumeration value="startValue" />
									<xsd:enumeration value="dirichletValue" />
								</xsd:restriction>
							</xsd:simpleType>
						</xsd:attribute>
					</xsd:complexType>
				</xsd:element>

        <!-- Optional paramteter for ersatz Material. Can be used with or without optimization  -->
        <xsd:element name="loadErsatzMaterial" type="DT_ErsatzMaterial" minOccurs="0"/>

        <!-- Parameters for specifying an optimization (optional) -->
        <xsd:element name="optimization" type="DT_Optimization" minOccurs="0"/>

        <!-- Root element of material database -->
        <xsd:element ref="mat:cfsMaterialDataBase" minOccurs="0" maxOccurs="1"/>
        
			</xsd:sequence>
			<!-- The simulation can be given an id/token/label/name that is written to info.xml if present. -->
			<xsd:attribute name="id" type="xsd:token" default="" use="optional" />
		</xsd:complexType>

		<!-- ***************************************************************** -->
		<!--   Occurence checks -->
		<!-- ***************************************************************** -->

		<!-- Make sure that for every region referenced in the PDE section -->
		<!-- a corresponding region has been designated in the domain part -->
		<xsd:keyref name="CS_RegionExists" refer="CS_DomainNamesRegion">
			<xsd:selector
				xpath="cfs:pdeList/cfs:LatticeBoltzmann/cfs:acoustic/cfs:region|cfs:pdeList/cfs:electrostatic/cfs:region|cfs:pdeList/cfs:magnetic/cfs:region|cfs:pdeList/cfs:mechanic/cfs:region|cfs:pdeList/cfs:piezo/cfs:region|cfs:pdeList/cfs:smooth/cfs:region|cfs:pdeList/cfs:heatConduction/cfs:region" />
			<!-- Simple selection of all PDEs does not work due to problems -->
			<!--- with auxilliary MPCCI PDE. What is a region in that case? -->
			<!--xsd:selector xpath="cfs:pdeList/*/cfs:region"/-->
			<xsd:field xpath="@name" />
		</xsd:keyref>

		<!-- NOTE: The following check is incomplete, since not all cases where
			nodes are referenced are contained in the selectors -->

		<!-- Make sure that for all 'nodes' referenced in PDE section also -->
		<!-- corresponding 'nodes' have been designated in the domain part -->
		<!--    <xsd:keyref name="CS_NodesExist1" refer="CS_DomainNamesNodes">
			<xsd:selector xpath="cfs:pdeList/*/cfs:bcsAndLoads/cfs:dirichletHom|cfs:pdeList/*/cfs:bcsAndLoads/cfs:dirichletInhom|cfs:pdeList/*/cfs:bcsAndLoads/cfs:neumannInhom"/>
			<xsd:field xpath="@name"/>
			</xsd:keyref>
			<xsd:keyref name="CS_NodesExist2" refer="CS_DomainNamesNodes">
			<xsd:selector xpath="cfs:pdeList/*/cfs:storeResults/cfs:nodeResults|cfs:pdeList/*/cfs:storeResults/cfs:nodeHistory"/>
			<xsd:field xpath="@saveNodes"/>
			</xsd:keyref>-->

		<!-- NOTE: The following check is probably incomplete, since not all
			cases where elements are referenced are contained in the selectors -->

		<!-- Make sure that for all 'nodes' referenced in PDE section also -->
		<!-- corresponding 'nodes' have been designated in the domain part -->
		<xsd:keyref name="CS_ElemsExist" refer="CS_DomainNamesElems">
			<xsd:selector
				xpath="cfs:pdeList/*/cfs:storeResults/cfs:elemResults" />
			<xsd:field xpath="@saveElems" />
		</xsd:keyref>

	</xsd:element>

</xsd:schema>
