Templates for Visual Studio Code
================================

For detailed information visit the [cfs-wiki page on vscode](https://gitlab.com/openCFS/cfs/-/wikis/vscode).

In this directory we collect some template files:
* Launch configuration [launch.json](./launch.json)
* Settings template [settings.json](./settings.json)

To use them:
1. Copy them where vscode wants them, usually `<source-dir>/.vscode`.
2. create a symlink `<source-dir>/.vscode/build-dir-link` pointing to your build directory.